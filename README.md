# GPT to Excel

A small tool to run a ChatGPT prompt against a lot of files. Once done, you can export a Spreadsheet.

Instructions how to run it:

Create an .env, add your `OPENAI_TOKEN` to it

`bundle install`

`ruby run.rb` to see all commands

`ruby run.rb new myproject` create a new project

`ruby run.rb list` see all projects

`ruby run.rb add myproject  . "**/*.rb"` Add all ruby files

`ruby run.rb single myproject [file]` Run ONE files

`ruby run.rb all myproject` Run all files

`ruby run.rb export myproject exports` Export my files to the output folder
