require "bundler/setup"
Bundler.require(:default)
require "json"
require "erb"
require "awesome_print"
require "optparse"
require 'csv'
require 'securerandom'

require_relative "lib/openai_chat"
require_relative "lib/file_finder"
require_relative "lib/file_analyzer"
require_relative "lib/models/open_ai_price"
require_relative "lib/models/project"
require_relative "lib/prompt_generator"
require_relative "lib/cli/projects"
require_relative "lib/cli/new_project"
require_relative "lib/cli/all"
require_relative "lib/cli/single"
require_relative "lib/cli/list"
require_relative "lib/cli/exporter"
require_relative "lib/cli/add_files"
require_relative "lib/exporters/csv_exporter"

Dotenv.load(".env")

raise "Please provide OPENAI_TOKEN key in .env file" if ENV["OPENAI_TOKEN"].nil?

# Print ASCII text "GPT CODE CHECK"

puts <<~ASCII_ART


_____ ______ _____   _____ ___________ _____   _____  _   _  _____ _____  _   __
                     ... GPT CODE CHECK ...
ASCII_ART

require "dry/cli"

# https://hanamimastery.com/episodes/37-dry-cli
module GptCodeCheck
  module CLI
    extend Dry::CLI::Registry

    register 'projects', Projects
    register 'new', NewProject
    register 'add', AddFiles
    register 'list', List
    register 'single', Single
    register 'all', AnalyzeAll
    register 'export', Exporter
  end
end

Dry::CLI.new(GptCodeCheck::CLI).call

