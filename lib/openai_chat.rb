require "json"
require "digest"
require "fileutils"

class OpenAIChat
  def initialize(api_key = nil)
    api_key ||= ENV['OPENAI_TOKEN']
    @client = OpenAI::Client.new(
      access_token: api_key,
      log_errors: true, # Highly recommended in development, so you can see what errors OpenAI is returning. Not recommended in production.
    )

    @prompts_dir = "data/prompts"
    @cache_dir = "data/responses"
    FileUtils.mkdir_p(@prompts_dir) unless File.directory?(@prompts_dir)
    FileUtils.mkdir_p(@cache_dir) unless File.directory?(@cache_dir)
  end

  def generate_response(prompt)
    md5 = Digest::MD5.hexdigest(prompt)
    prompt_file = File.join(@prompts_dir, "#{md5}.txt")
    cache_file = File.join(@cache_dir, "#{md5}.json")

    File.open(prompt_file, "w+") { |f| f.write(prompt) }

    response = if File.exist?(cache_file)
        puts "Using cached response for prompt: #{prompt[0..50]}..."
        JSON.parse(File.read(cache_file))
      else
        puts "No cached response found for prompt: #{prompt[0..50]}... Generating new response..."
        new_call_response = call_openai_api(prompt)
        save_to_cache(cache_file, new_call_response)
        new_call_response
      end


    content = response["choices"].first["message"]["content"]
    content_without_code_tags = content.gsub(/^```json|```$/, '')
    
  return {
      items: JSON.parse(content_without_code_tags),
      usage: response['usage'],
      model: response['model'],
    }

  end

  private

  def call_openai_api(prompt)
    response = @client.chat(
      parameters: {
        model: "gpt-4-turbo",
        messages: [{ role: "user", content: prompt }],
        temperature: 0.7,
        max_tokens: 500,
      },
    )

    JSON.parse(response.to_json)
  end

  def save_to_cache(cache_file, response)
    File.open(cache_file, "w") { |f| f.write(JSON.pretty_generate(response)) }
  end
end
