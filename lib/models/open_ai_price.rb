class OpenAiPrice
  REQUEST_COST_PER_MILLION = 10.00
  RESPONSE_COST_PER_MILLION = 30.00

  attr_reader :request_tokens, :response_tokens

  def self.request(request_tokens)
    new(request_tokens: request_tokens)
  end

  def initialize(request_tokens: nil, response_tokens: nil)
    @request_tokens = request_tokens
    @response_tokens = response_tokens
  end

  def response(response_tokens)
    @response_tokens = response_tokens
    self
  end

  def price
    total_tokens = @request_tokens * REQUEST_COST_PER_MILLION + @response_tokens * RESPONSE_COST_PER_MILLION
    cost = total_tokens.to_f / 1_000_000
    cost.round(2)
  end

  def print
    "Estimated ChatGPT API Cost: US$#{price}"
  end
end
