class Project
  attr_reader :name, :directory, :filesets, :files

  def self.create(name)
    file_path = project_folder(name)
    raise "Project '#{name}' already exists!" if File.exist?(file_path)

    project_data = {
      name: name,
      filesets: [],
      files: []
    }

    File.open(file_path, "w") { |f| f.write(JSON.pretty_generate(project_data)) }
    new(name)
  end

  def self.all
    projects_folder = 'data/projects'
    Dir.glob("#{projects_folder}/*.json").map do |file_path|
      project_name = File.basename(file_path, ".json")
      load(project_name)
    end
  rescue StandardError => e
    puts "Failed to load projects: #{e.message}"
    []
  end

  def self.load(name)
    file_path = project_folder(name)
    raise "Project '#{name}' doesn't exist!" unless File.exist?(file_path)

    project_data = JSON.parse(File.read(file_path), symbolize_names: true)
    new(name, project_data[:filesets], project_data[:files])
  end

  def initialize(name, filesets = [], files = [])
    @name = name
    @filesets = filesets
    @files = files
  end

  def add_fileset(directory, file_pattern)
    fileset_id = generate_fileset_id
    files = Dir.glob(File.join(directory, file_pattern))  # Collect all files matching the pattern in the directory

    files_info = files.map do |file_path|
      contents = File.read(file_path)
      md5 = Digest::MD5.hexdigest(contents)
      {
        fileset: fileset_id,  # Associate this file with the current fileset
        file: file_path.sub("#{directory}/", ""),  # Store relative path
        text_tokens: contents.split.size,
        md5: md5,
        analysis: {}  # Placeholder for future analysis
      }
    end

    # Add the new fileset information
    @filesets << {id: fileset_id, directory: directory, pattern: file_pattern}
    @files.concat(files_info)  # Add new files to the project's file list
    save
    fileset_id
  end


  def list_filesets
    @filesets
  end

  def remove_fileset(fileset_id)
    @filesets.reject! { |fs| fs[:id] == fileset_id }
    save
  end

  def save
    file_path = self.class.project_folder(@name)
    File.open(file_path, "w") { |f| f.write(JSON.pretty_generate(to_json)) }
  end


  def print_files
    files.each do |file_info|
      fileset = filesets.find { |fs| fs[:id] == file_info[:fileset] }
      next unless fileset
      directory = fileset[:directory]
      relative_path = file_info[:file].sub("#{directory}/", "")
      items = file_info[:analysis].size
      tokens = file_info[:text_tokens]

      puts "#{items.to_s.rjust(3)} items | #{tokens.to_s.rjust(4)} Tokens | #{relative_path}"
    end

    print_price_breakdown
  end

  def reindex
    filesets.each do |fileset|
      directory = fileset[:directory]
      pattern = fileset[:pattern]
      files.select { |f| f[:fileset] == fileset[:id] }.each do |file_info|
        file_path = File.join(directory, file_info[:file])
        # Reindex logic here, updating the file info as necessary
      end
    end
    save
  end

  def each_file
    files.each do |file_info|
      fileset = filesets.find { |fs| fs[:id] == file_info[:fileset] }
      next unless fileset
      directory = fileset[:directory]
      abspath = File.join(directory, file_info[:file])
      yield(abspath, file_info) if block_given?
    end
  end

  def process_each(rerun = false)
    relevant_files = rerun ? files : files_pending
    relevant_files.each do |file_info|
      fileset = filesets.find { |fs| fs[:id] == file_info[:fileset] }
      next unless fileset
      directory = fileset[:directory]
      one_file(directory, file_info[:file]) { |abspath, info| yield(abspath, info) if block_given? }
    end
  end

  def one_file(directory, file)
    raise ArgumentError, "Block must be provided" unless block_given?

    file_info = files.find { |info| File.join(directory, info[:file]) == File.join(directory, file) }
    raise "Couldn't find the file #{file} in a list of #{files.size} files." if file_info.nil?

    abspath = File.join(directory, file)
    yield(abspath, file_info)
    save
  end
  

  def print_price_breakdown(rerun = false)
    puts "-------------"

    puts "Price for the whole project"
    print_price_for_files(files)

    return if rerun
    
    puts "-------------"
    puts "Price for the files left to do"
    print_price_for_files files_pending

    puts "-------------"
  end

  def files_pending
    files.select{|data| data[:analysis].empty? }
  end

  def size_pending
    files_pending.size
  end

  def size
    files.size
  end

  private


  def print_price_for_files(relevant_files)
    puts "- Files: #{relevant_files.size}"
    text_tokens = relevant_files.sum { |file| file[:text_tokens].to_i }
    puts "- Text tokens: #{text_tokens}"
    template_tokens = PromptGenerator.new(nil).template_tokens * relevant_files.size
    response_tokens = 300 * relevant_files.size
    puts "- Template tokens: #{template_tokens}"
    puts "- Average response tokens: #{response_tokens}"

    all_query_tokens = template_tokens + response_tokens
    price = OpenAiPrice.request(text_tokens + template_tokens).response(response_tokens).price
    puts "- Total cost: #{all_query_tokens + response_tokens} tokens, price for everything USD $#{price}"
  end

  def self.project_folder(name)
    raise "Name must be set" if name.nil?
    "data/projects/#{name}.json"
  end

  def to_json
    {
      name: @name,
      filesets: @filesets,
      files: @files
    }
  end

  def generate_fileset_id
    date_prefix = Date.today.strftime("%Y-%m-%d")  # Formats the current date as "YYYY-MM-DD"
    random_hex = SecureRandom.hex(4)  # Generates a shorter hex string (8 characters)
    "#{date_prefix}_#{random_hex}"  # Combines them with an underscore
  end

end
