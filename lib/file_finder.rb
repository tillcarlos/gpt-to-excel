
class FileFinder
  def initialize(folder, project_name, file_pattern)
    @folder = folder
    @project_name = project_name
    @file_pattern = file_pattern
  end

  def analyze_files
    file_path = "data/projects/#{@project_name}.json"
    if File.exist?(file_path)
      puts "File #{file_path} exists already! Do you want to overwrite it? (yes/no)"
      answer = $stdin.gets.chomp.downcase
    
      raise "Operation aborted. File already exists and won't be overwritten." unless answer == 'yes'
    end

    files = Dir.glob(File.join(@folder, @file_pattern))

    files_info = files.map do |file|
      contents = File.read(file_path)
      md5 = Digest::MD5.hexdigest(contents)
      {
        file: file.sub("#{@folder}/", ""), # Relative path
        text_tokens: contents.split.size,
        response_tokens: 0,
        md5: md5,
        analysis: {} # Placeholder for future analysis
      }
    end

    data = {
      directory: @folder,
      projectname: @project_name,
      pattern: @file_pattern,
      files: files_info
    }

    File.open(file_path, "w") { |f| f.write(JSON.pretty_generate(data)) }

    files_info
  end

  def list_files_and_tokens(files_info)
    total_tokens = 0
    puts "Files found:"
    files_info.each do |file_info|
      relative_path = file_info[:file].sub("#{file_info[:directory]}/", "")
      puts "#{relative_path} - #{file_info[:tokens]} tokens"
      total_tokens += file_info[:tokens]
    end
    puts "Total tokens: #{total_tokens}"

    # Calculate ChatGPT API cost
    cost_per_million_tokens = 10.00
    tokens_processed = total_tokens
    cost = (tokens_processed.to_f / 1_000_000) * cost_per_million_tokens

    puts "______"
    price = OpenAiPrice.request(total_tokens).response(500).price
    puts "Estimated ChatGPT API Cost: US$#{price}"
  end
end
