module GptCodeCheck
  module CLI
    class AnalyzeAll < Dry::CLI::Command
      desc 'Analyses one file from a project'

      argument :project, type: :string, required: true, desc: "Project name"
      argument :rerun, type: :boolean, required: false, desc: "Rerun existing files"


      # attr_reader :transformation, :repository
      def initialize
        # @repository = Repositories::Episodes.new
        # @transformation = Transformations::Touch.new
      end

      def call(project:, file: nil, rerun: false, **)
        p = Project.load(project)

        if file.nil?
          puts "⚠️ You are about to run this on all #{p.size} files in a repository. ⚠️"
          puts " --> ⚠️ Also: this will overwrite exeisting run!!!  ⚠️" if rerun
          puts ""
          p.print_price_breakdown(rerun)
          puts "Are you sure? (write 'yes' to continue)"
          answer = $stdin.gets.chomp.downcase
          raise "Operation aborted. Please say 'yes'!" unless answer == 'yes'
        end

        progressbar = ProgressBar.create(total: p.size_pending, 
          :format         => "%c/C | %a %b\u{15E7}%i %p%% %t",
          :progress_mark  => ' ',
          length: 80,
          :remainder_mark => "\u{FF65}")
        
        p.process_each(rerun) do |abspath, data|
          progressbar.increment

          puts "Using file: #{abspath}"

          prompt = PromptGenerator.new(abspath).merge
      
          airesponse = OpenAIChat.new.generate_response(prompt)
          ap airesponse[:items]
      
          data[:analysis] = {
            items: airesponse[:items], 
            usage: airesponse[:usage], 
            model: airesponse[:model]
          }
          
          # after this run, the method saves the project
        end
      end


    end
  end
end