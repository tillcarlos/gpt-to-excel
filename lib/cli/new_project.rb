module GptCodeCheck
  module CLI
    class NewProject < Dry::CLI::Command
      desc 'Creates a new project (lists all files from a subfolder)'

      argument :name, type: :string, required: true, desc: "Project name"

      # attr_reader :transformation, :repository
      def initialize
        # @repository = Repositories::Episodes.new
        # @transformation = Transformations::Touch.new
      end

      def call(name:, **)
        Project.create(name)
        puts "Created project"
      end
    end
  end
end