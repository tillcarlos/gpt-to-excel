module GptCodeCheck
  module CLI
    class List < Dry::CLI::Command
      desc 'Lists all available projects. And some stats.'

      # argument :episode, type: :integer, required: true, desc: "Episodes ID to unshot"


      # attr_reader :transformation, :repository
      def initialize
        # @repository = Repositories::Episodes.new
        # @transformation = Transformations::Touch.new
      end

      def call()
        
        projects = Project.all

        puts "+++++++++++++++++++++++++++"
        puts "Found #{projects.size} projects"

        projects.each do |project|
          puts "------------------"
          puts "Project: #{project.name}"
          puts "Filesets: #{project.filesets.size}"
          project.filesets.each do |fileset|
            puts "Fileset ID: #{fileset[:id]} - Directory: #{fileset[:directory]} - Pattern: #{fileset[:pattern]}"
          end
          analized = project.files.count { |file| file[:analysis] && file[:analysis].any? }
          puts "Number of Files: #{project.files.size}"
          puts "Files with Analysis: #{analized}"
          puts "Files to do: #{project.files.size - analized}"
          puts "------------------"
        end
        
        puts "+++++++++++++++++++++++++++"

      end
    end
  end
end