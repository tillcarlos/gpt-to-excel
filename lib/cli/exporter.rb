module GptCodeCheck
  module CLI
    class Exporter < Dry::CLI::Command
      desc 'Lists all available projects. And some stats.'

      argument :project, type: :string, required: true, desc: "Project name"
      argument :directory, type: :string, required: true, desc: "Export directory"


      # attr_reader :transformation, :repository
      def initialize
        # @repository = Repositories::Episodes.new
        # @transformation = Transformations::Touch.new
      end

      def call(project:, directory:, **)
        project = Project.load(project)

        exporter = Exporters::CsvExporter.new(project, directory)

        exporter.export

        puts "+++++++++++++++++++++++++++"

      end
    end
  end
end