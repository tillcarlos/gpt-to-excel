module GptCodeCheck
  module CLI
    class Single < Dry::CLI::Command
      desc 'Analyses one file from a project'

      argument :project, type: :string, required: true, desc: "Project name"
      argument :file, type: :string, required: false, desc: "File you want to analyze"


      # attr_reader :transformation, :repository
      def initialize
        # @repository = Repositories::Episodes.new
        # @transformation = Transformations::Touch.new
      end

      def call(project:, file: nil, **)
        p = Project.load(project)

        if file.nil?
          p.print_files

          puts "You didn't specify the file. Which one do you want to run? "
          file = $stdin.gets.chomp.downcase
        end

        puts "Using file: #{file}"

        p.one_file(file) do |abspath, data|
          # data is a hash with these keys
          # abspath, analysis, md5
          prompt = PromptGenerator.new(abspath).merge
      
          airesponse = OpenAIChat.new.generate_response(prompt)
          ap airesponse[:items]
      
          # Update the analysis data for the selected file in the project JSON
          data[:analysis] = {
            items: airesponse[:items], 
            usage: airesponse[:usage], 
            model: airesponse[:model]
          }
          # after this run, the method saves the project
        end
      end


    end
  end
end