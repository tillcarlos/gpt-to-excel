module GptCodeCheck
  module CLI
    class AddFiles < Dry::CLI::Command
      desc 'Creates a new project (lists all files from a subfolder)'

      argument :name, type: :string, required: true, desc: "Project name"
      argument :folder, type: :string, required: true, desc: "Root folder of the project"
      argument :pattern, type: :string, required: true, desc: "File pattern, like \"**/*.rb\" (quotes important)"


      # attr_reader :transformation, :repository
      def initialize
        # @repository = Repositories::Episodes.new
        # @transformation = Transformations::Touch.new
      end

      def call(name:, folder:, pattern:, **)
        project = Project.load(name)
        id = project.add_fileset(folder, pattern)
        ap "Added fileset #{id} to project #{name}"
      end
    end
  end
end