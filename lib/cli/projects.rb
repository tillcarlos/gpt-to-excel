module GptCodeCheck
  module CLI
    class Projects < Dry::CLI::Command
      desc 'Lists all available projects. And some stats.'

      # argument :episode, type: :integer, required: true, desc: "Episodes ID to unshot"


      # attr_reader :transformation, :repository
      def initialize
        # @repository = Repositories::Episodes.new
        # @transformation = Transformations::Touch.new
      end

      def call()

        projects = Dir.glob("data/projects/*.json")

        puts "+++++++++++++++++++++++++++"
        puts "Found #{projects.size} projects"

        projects.each do |project|
          puts "------------------"
          data = JSON.parse(File.read(project), symbolize_names: true)
          puts "Project: #{data[:projectname]}"
          puts "Folder: #{data[:directory]}"
          puts "File Pattern: #{data[:file_pattern]}"
          puts "Number of Files: #{data[:files].size}"
          puts "Files with Analysis: #{data[:files].count { |file| file[:analysis].any? }}"
          puts "------------------"
        end
        puts "+++++++++++++++++++++++++++"

      end
    end
  end
end