module Exporters
  class CsvExporter
    attr_reader :project, :directory

    def initialize(project, directory)
      @project = project
      @directory = directory
    end

    require 'csv'

    def export
      # TODO: Make this a readable date.
      timestamp = Time.now.utc.strftime('%Y-%m-%d_%H-%M_utc')
      filename = "#{project.name}_#{timestamp}.csv"
      filepath = File.join(directory, filename)
    
      CSV.open(filepath, 'w') do |csv|
        # Define CSV headers
        csv << ["File", "Text Tokens", "Neutral Analysis", "Neutral Count", "Minor Analysis", "Minor Count", "Critical Analysis", "Critical Count", "Severity", "Certainty", "Tokens"]
    
        project.each_file do |abspath, data|
          # Parsing data structure
          file_data = data[:file]
          text_tokens = read_column(data, :text_tokens)
          neutral_analysis = read_column(data, :analysis, :items, :neutral)
          neutral_count = neutral_analysis.split('; ').count
          minor_analysis = read_column(data, :analysis, :items, :minor)
          minor_count = minor_analysis.split('; ').count
          critical_analysis = read_column(data, :analysis, :items, :critical)
          critical_count = critical_analysis.split('; ').count
          severity = read_column(data, :analysis, :items, :severity)
          certainty = read_column(data, :analysis, :items, :certainty)
          tokens = read_column(data, :analysis, :usage, :total_tokens)
    
          # Write data to CSV
          csv << [file_data, text_tokens, neutral_analysis, neutral_count, minor_analysis, minor_count, critical_analysis, critical_count, severity, certainty, tokens]
        end
      end
    
      puts "Data exported to #{filepath}"
    end
    

    def read_column(data, *keys, default: "-")
      begin
        value = data.dig(*keys)
        if value.is_a?(Array)
          value.join("\n\n")  # Join with newline and separator
        else
          value || default
        end
      rescue
        default
      end
    end
    
    

    
  end
end
