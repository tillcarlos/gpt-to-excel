class FileAnalyzer
  attr_reader :project_data, :project_name, :file

  def initialize(project_name, file)
    @project_name = project_name
    @file = file
    load_project_data
  end

  def analyze_file
    if file.nil?
      puts "Available files for analysis:"
      ap possible_files

      loop do
        print "Which file do you want to analyze? Type here: "
        @file = $stdin.gets.chomp.downcase

        break if possible_files.include?(file)

        puts "Invalid file. Please choose from the available options."
      end
    end

    pg = PromptGenerator.new(project_name, file)
    prompt = pg.merge

    puts "+++++++++++++++++++++++++++"
    ap prompt
    puts "+++++++++++++++++++++++++++"

    openai = OpenAIChat.new
    analysis = openai.generate_response(prompt)
    ap analysis

    # Update the analysis data for the selected file in the project JSON
    update_analysis(analysis[:items], analysis[:usage], analysis[:model])
  end

  private

  def load_project_data
    file_path = "data/projects/#{project_name}.json"
    @project_data = JSON.parse(File.read(file_path), symbolize_names: true)
  end

  def possible_files
    @possible_files ||= project_data[:files].map { |item| item[:file] }
  end

  def update_analysis(items, usage, model)
    # Find the file entry and update its analysis
    project_data[:files].each do |file_info|
      next unless file_info[:file] == file

      file_info[:analysis] = items
      file_info[:usage] = usage
      file_info[:model] = model
    end

    # Save updated project data back to the JSON file
    File.write(file_path, JSON.pretty_generate(project_data))
  end

  def file_path
    "data/projects/#{project_name}.json"
  end
end
