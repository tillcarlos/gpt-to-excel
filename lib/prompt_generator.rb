
class PromptGenerator
  def initialize(file_name)
    @file_name = file_name
  end

  def merge
    # Read the code from the file
    code_file_path = File.join(@file_name)
    @code = File.read(code_file_path)

    # Merge code into the prompt template
    prompt = ERB.new(read_template).result(binding)

    # Output the merged prompt
    prompt
  end

  def read_template
    File.read("prompts/world_class_dev.erb")
  end

  def template_tokens
    read_template.split.size
  end

end
